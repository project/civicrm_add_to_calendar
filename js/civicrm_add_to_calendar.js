(function ($) {
  'use strict';
  Drupal.behaviors.civicrmAddToCalendar = {
    attach: function (context, settings) {
      $('.iCal_links-section', context).once('civicrm-add-to-calendar', function () {
        // Replaces iCal links with Add to Calendar dropdown.
        $(this).empty().append($('#civicrm-add-to-calendar-wrapper .crm-accordion-wrapper'));
      });
    }
  };
}(jQuery));
