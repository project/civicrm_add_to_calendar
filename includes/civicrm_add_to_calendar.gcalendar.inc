<?php
/**
 * @file
 * Google Calendar functions for CiviCRM Add to Calendar module.
 */

define('CIVICRM_GCALENDAR_BASE_PATH', 'https://www.google.com/calendar/render');

/**
 * Builds a Google Calendar URL for the given CiviCRM event ID.
 *
 * @param int $event_id
 *   The event ID.
 *
 * @return string
 *   The Google Calendar template URL.
 */
function civicrm_add_to_calendar_build_gcalendar_url($event_id) {
  global $base_url;

  $events = civicrm_api3('Event', 'get', [
    'id' => $event_id,
    'api.LocBlock.get' => [
      'api.Address.get' => [
        'api.StateProvince.get' => [
          'api.Country.get' => 1,
        ],
      ],
    ],
  ]);

  if (!$events['count']) {
    return FALSE;
  }

  $event = $events['values'][$event_id];
  if (!$dates = civicrm_add_to_calendar_format_gcalendar_dates($event)) {
    return FALSE;
  }

  $args = [
    'action' => 'TEMPLATE',
    'text' => check_plain($event['title']),
    'dates' => $dates,
    'details' => civicrm_add_to_calendar_format_gcalendar_details($event),
    'sprop' => 'website:' . check_plain($base_url),
    'website' => url(current_path(), [
      'absolute' => TRUE,
      'query' => ['id' => check_plain($event_id), 'reset' => 1],
    ]),
  ];

  if ($location = civicrm_add_to_calendar_format_gcalendar_location($event)) {
    $args['location'] = $location;
  }

  return url(CIVICRM_GCALENDAR_BASE_PATH, ['query' => $args]);
}

/**
 * Builds event dates for Google Calendar from the given event info.
 *
 * @param array $event
 *   The event information array from CiviCRM API.
 *
 * @return string
 *   Start & end dates formatted as YYYYMMDDToHHmmSSZ/YYYYMMDDToHHmmSSZ.
 */
function civicrm_add_to_calendar_format_gcalendar_dates(array $event) {
  if (empty($event['start_date'])) {
    return '';
  }

  $dates = [$event['start_date']];

  // If end date is not available, set start date as default.
  $dates[] = empty($event['end_date']) ? $event['start_date'] : $event['end_date'];

  // Get site timezone.
  $tz = variable_get('date_default_timezone', @date_default_timezone_get());
  foreach ($dates as $i => $date) {
    // Convert event date to GMT.
    $date = new DateTime($date, new DateTimeZone($tz));
    $date->setTimezone(new DateTimeZone('GMT'));
    $dates[$i] = $date->format('Ymd\THi00\Z');
  }

  return implode('/', $dates);
}

/**
 * Builds event details for Google Calendar from the given event info.
 *
 * @param array $event
 *   The event information array from CiviCRM API.
 *
 * @return string $formatted
 *   The formatted and filtered event description.
 */
function civicrm_add_to_calendar_format_gcalendar_details(array $event) {
  $formatted = '';

  if (!empty($event['description']) && variable_get('civicrm_add_to_calendar_gcalendar_desc_enabled', TRUE)) {
    $format_id = variable_get('civicrm_add_to_calendar_gcalendar_desc_filter_format', 'civicrm_add_to_calendar_gcalendar');
    $formatted = check_markup($event['description'] . '<br>', $format_id);
  }

  if (variable_get('civicrm_add_to_calendar_gcalendar_civi_link_enabled', TRUE)) {
    // Appends a link to the Civi event at the bottom of the description.
    $formatted .= t('See event page: <a href="@url">@url</a>', [
      '@url' => url('civicrm/event/info', [
        'query' => ['id' => check_plain($event['id']), 'reset' => 1],
        'absolute' => TRUE,
      ]),
    ]);
  }

  return $formatted;
}

/**
 * Builds location string for Google Calendar event link.
 *
 * @param array $event
 *   The event information array from CiviCRM API.
 *
 * @return string
 *   The formatted location string.
 */
function civicrm_add_to_calendar_format_gcalendar_location(array $event) {
  // Merging nested event data.
  $addr = $event;
  foreach (['LocBlock', 'Address', 'StateProvince', 'Country'] as $obj_type) {
    $key = 'api.' . $obj_type . '.get';
    if (empty($addr[$key]) || !$addr[$key]['count']) {
      if (!isset($addr['location_type_id'])) {
        return FALSE;
      }

      break;
    }

    $addr = $addr[$key]['values'][0] + $addr;
  }

  // The following array is a template to build the location string. Items will
  // be separated by commas, while subitems will be separated by whitespaces.
  $format = [
    ['street_address'],
    ['supplemental_address_1'],
    ['supplemental_address_2'],
    ['city', 'abbreviation', 'postal_code'],
    ['name'],
  ];

  $lines = [];
  foreach ($format as $fields) {
    $line = [];
    foreach ($fields as $field) {
      if (!empty($addr[$field])) {
        $line[] = $addr[$field];
      }
    }

    if (!empty($line)) {
      $lines[] = implode(' ', $line);
    }
  }

  return check_plain(implode(', ', $lines));
}
