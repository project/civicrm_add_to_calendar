<?php
/**
 * @file
 * Admin UI functions for CiviCRM Add to Calendar module.
 */

/**
 * Builds Add to Calendar configuration form.
 */
function civicrm_add_to_calendar_config_form(&$form, &$form_state) {
  $form['gcalendar'] = [
    '#title' => t('Google Calendar link'),
    '#type' => 'fieldset',
  ];

  $prefix = 'civicrm_add_to_calendar_gcalendar';
  $form['gcalendar'][$prefix . '_desc_enabled'] = [
    '#title' => t('Include CiviCRM event description'),
    '#type' => 'checkbox',
    '#default_value' => variable_get($prefix . '_desc_enabled', TRUE),
  ];

  $formats = [];
  foreach (filter_formats() as $key => $ff) {
    $formats[$key] = $ff->name;
  }

  $form['gcalendar'][$prefix . '_desc_filter_format'] = [
    '#title' => t('Filter format for event description'),
    '#type' => 'select',
    '#default_value' => variable_get($prefix . '_desc_filter_format', $prefix),
    '#options' => $formats,
    '#description' => t('Text formats can be configured <a href="!url">here</a>.', [
      '!url' => url('admin/config/content/formats'),
    ]),
    '#states' => [
      'visible' => [
        ':input[name="' . $prefix . '_desc_enabled"]' => [
          'checked' => TRUE,
        ],
      ],
    ],
  ];

  $form['gcalendar'][$prefix . '_civi_link_enabled'] = [
    '#title' => t('Include a link to CiviCRM event'),
    '#description' => t('It will be displayed at the bottom of Google Calendar event description as follows: <em>See event page: [link-to-event]</em>'),
    '#type' => 'checkbox',
    '#default_value' => variable_get($prefix . '_civi_link_enabled', TRUE),
  ];

  return system_settings_form($form);
}
