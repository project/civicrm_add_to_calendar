<?php
/**
 * @file
 * Main module file for CiviCRM Add to Calendar module.
 */

/**
 * Implements hook_help().
 */
function civicrm_add_to_calendar_help($path, $arg) {
  if ($path != 'admin/help#civicrm_add_to_calendar') {
    return;
  }

  $filepath = dirname(__FILE__) . '/README.md';
  if (file_exists($filepath)) {
    $readme = file_get_contents($filepath);
  }
  else {
    $filepath = dirname(__FILE__) . '/README.txt';
    if (file_exists($filepath)) {
      $readme = file_get_contents($filepath);
    }
  }
  if (!isset($readme)) {
    return NULL;
  }
  if (module_exists('markdown')) {
    $filters = module_invoke('markdown', 'filter_info');
    $info = $filters['filter_markdown'];

    if (function_exists($info['process callback'])) {
      $help = $info['process callback']($readme, NULL);
    }
    else {
      $help = '<pre>' . $readme . '</pre>';
    }
  }
  else {
    $help = '<pre>' . $readme . '</pre>';
  }

  return $help;
}

/**
 * Implements hook_theme().
 */
function civicrm_add_to_calendar_theme($existing, $type, $theme, $path) {
  return [
    'civicrm_add_to_calendar_dropdown' => [
      'template' => 'templates/civicrm-add-to-calendar-dropdown',
      'variables' => ['title' => '', 'links' => []],
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function civicrm_add_to_calendar_menu() {
  $items = [];

  $items['admin/config/services/civicrm-add-to-calendar'] = [
    'title' => 'CiviCRM Add to Calendar',
    'description' => 'Configure CiviCRM Add to Calendar',
    'access arguments' => ['administer site configuration'],
    'page callback' => 'drupal_get_form',
    'page arguments' => ['civicrm_add_to_calendar_config_form'],
    'file' => 'includes/civicrm_add_to_calendar.admin.inc',
  ];

  return $items;
}

/**
 * Implements hook_civicrm_alterContent().
 */
function civicrm_add_to_calendar_civicrm_alterContent(&$content, $context, $tplName, &$object) {
  switch ($tplName) {
    case 'CRM/Event/Page/EventInfo.tpl':
      $key = '_id';
      break;

    case 'CRM/Event/Form/Registration/ThankYou.tpl':
      $key = '_eventId';
      break;

    default:
      return;
  }

  if (empty($object->{$key}) || strpos($content, 'iCal_links-section') === FALSE) {
    return;
  }

  module_load_include('inc', 'civicrm_add_to_calendar', 'includes/civicrm_add_to_calendar.gcalendar');

  $event_id = $object->{$key};
  $links = [];
  $links['ical'] = [
    'title' => t('iCalendar'),
    'path' => url('civicrm/event/ical', [
      'query' => ['reset' => 1, 'id' => check_plain($event_id)],
    ]),
  ];

  if ($path = civicrm_add_to_calendar_build_gcalendar_url($event_id)) {
    $links['gcalendar'] = [
      'title' => t('Google Calendar'),
      'path' => $path,
    ];
  }

  $base_path = drupal_get_path('module', 'civicrm_add_to_calendar');
  $btn_container = [
    '#type' => 'container',
    '#attributes' => [
      'id' => 'civicrm-add-to-calendar-wrapper',
      'class' => ['element-invisible'],
    ],
    'add_to_calendar' => [
      '#theme' => 'civicrm_add_to_calendar_dropdown',
      '#title' => t('Add to Calendar'),
      '#links' => $links,
    ],
    '#attached' => [
      'css' => [$base_path . '/css/civicrm_add_to_calendar.css'],
      'js' => [$base_path . '/js/civicrm_add_to_calendar.js'],
    ],
  ];

  $content .= render($btn_container);
}
