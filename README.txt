CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

CiviCRM Add to Calendar upgrades event CiviCRM iCal links to a better looking
dropdown that includes a link to Google Calendar.


REQUIREMENTS
------------

This module requires the following modules:

 * CiviCRM (civicrm) (https://civicrm.org/download)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

Navigate to administer >> modules. Enable CiviCRM Add to Calendar.


CONFIGURATION
-------------

After enabling the module, make sure to set up settings at
/admin/config/services/civicrm-add-to-calendar
