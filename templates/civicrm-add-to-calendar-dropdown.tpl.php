<?php
/**
 * @file
 * Theme Add to Calendar dropdown.
 *
 * - $title: The dropdown title.
 * - $links: An array of links. Each item is keyed as follows:
 *   - title: The link title
 *   - path: The link path
 */
?>
<div class="crm-accordion-wrapper collapsed">
  <div class="crm-accordion-header">
    <i class="fa fa-calendar"></i>
    <span><?php print $title; ?></span>
  </div>
  <div class="crm-accordion-body" style="display:none;">
    <?php foreach ($links as $link): ?>
      <a href="<?php print $link['path']; ?>"
         class="civicrm-add-to-calendar-link"
         target="_blank"><?php print $link['title']; ?></a>
    <?php endforeach; ?>
  </div>
</div>
